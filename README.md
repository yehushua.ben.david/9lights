# 9lights.sh

Fer rouleds, set bash game of turning on all the lights in a 3x3 grid. Each cell in the grid can be toggled between on (1) and off (0). Toggling a cell also toggles its adjacent cells. The player moves around the grid using arriow keys and toggles the cells using the 't' key.

@```bash
chmod +x 9lights.sh
```

## Overview

The `9lights.sh` script is a simple bash game where the objective is to switch on all the lights in a 3x3 grid. Each cell in the grid can be toggled between on (1) and off (0). Toggling a cell also toggles its adjacent cells. The player moves around the grid using arriow keys and toggles the cells using the 't' key.


## How to Play

1. **Objective**: Turn on all the lights in the 3x3 grid.
2. **Controls:**
   - Use the arrow keys to move the cursor around the grid.
   - Press `t` to toggle the current cell and its adjacent cells.
   - Press `x` to exit the game.


## Setup
Ensure you have the necessary permissions to execute the script. You may need to modify the file permissions to make it executable:

```bash
#chmod +x 9lights.sh
```


## Usage

Run the script from the terminal:

```bash
./9lights.sh
```


## Script Breakdown
1. **Initialization:**
   - The script begins by hiding the cursor using `tput civis`.
   - Ensures it will be restored upon exit with `tput cnorm`.
   - Initializes the grid `brd` with all lights off (0).

. **Game Loop***
   - Continuously redraws the grid and waits for user input.
   - Moves the cursor or toggles cells based on the input.

. **Functions***
   - `cleanup()`: Ensures the cursor is visible again when the script exits.
   - `toggle():` Toggles the selected cell and its adjacent cells.
   - `toggleAt():` Toggles a specific cell.
   - `drawme():` Draws the current state of the grid and checks for a win condition.

. **Grid Representation***
   - The grid is represented as a one-dimensional array of nine Elements (`brd`.)
   - The cursor position is tracked by the variable `pos.`
   - Cells are toggled by calculating their positions in the array.

. **Winning Condition ***
   - The game checks if all cells in the grid are turned on (1).
   - If all cells are on, the game congratulates the player and exits.

## Example
Here's a brief example of how the game might look in the terminal:

`bash
Switch on everything.
Use arrow keys to move and 't' to toggle.
1 0 0
0 1 0
0 0 1
Done in 5 moves! Bravo!
```

## Notes
- The game is implemented entirely in bash and uses basic terminal control commands.
- It provides a simple yet challenging puzzle that can be played directly from the command line.

Enjoy playing and good luck!
