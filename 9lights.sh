#!/bin/bash 

function cleanup() {
    tput cnorm
}

trap cleanup EXIT

tput civis

moves=0
pos=4
posx=1
posy=1
brd=(0 0 0 0 0 0 0 0 0)
toggle(){
  ((moves++))
  posx=$((pos % 3 ))
  posy=$((pos/3))
  toggleAt $pos
  if ((posx<2)) ; then 
    toggleAt $(( pos + 1  ))
  fi
  if ((posx>0)) ; then 
    toggleAt $(( pos - 1  ))
  fi
  if ((posy>0)) ; then 
    toggleAt $(( pos - 3  ))
  fi
  if ((posy<2)) ; then 
    toggleAt $(( pos + 3  ))
  fi



} 
toggleAt(){
  lp=$1
  v=${brd[$lp]}
  brd[$lp]=$(((v+1)%2))
}
drawme() {
echo -e "\033[H\033[JSwitch on every thing.\nUse arrow keys to move and 't' to toggle."
  for (( i=0 ; i < 9 ; i++ )) ; do
  if ((i==pos)) ; then echo -en "\033[41m${brd[$i]}\033[m" 
  else 
     echo -n ${brd[$i]} ; 
  fi
  if (((i+1)%3==0)) ; then echo ; fi    
done
echo "" 
win=0
for ((i=0;i<9;i++)); do
((win += brd[i])) 
done 
if (( win == 9 )) ; then 
echo "Done in $moves! Bravo !"
exit
fi

}
while true 
do     
  if ! [ "$t" == "skip" ] ; then 
   drawme
  fi 
  
read -sn 1 -t 1  t || t="skip"     
  case "$t" in
      A) (( pos=((pos + 9 ) - 3 ) %9  )) ;; #UP   
      B) (( pos=((pos + 9 ) +3  ) %9  )) ;; #DOWN
      C) (( pos=((pos + 9 ) +10 ) %9  )) ;; #RIGHT   
      D) (( pos=((pos + 9 ) - 1 ) %9  )) ;; #LEFT
      t) toggle ;;
      x) exit ;; 
      *) t="skip" ;;  
  esac; 
done

